#ifdef __cplusplus
extern "C" {
#endif
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef __cplusplus
}
#endif

#include "ppport.h"
#include "XTime.h"


MODULE = Chandra::Time		PACKAGE = Chandra::Time		

char *
convert_time( time_in, ts_in, tf_in, ts_out, tf_out )
  char *time_in
  char *ts_in
  char *tf_in
  char *ts_out
  char *tf_out
 CODE:
  New( 0, RETVAL, 255, char );
  axTime3( time_in, ts_in, tf_in, ts_out, tf_out, RETVAL);
 OUTPUT:
  RETVAL
 CLEANUP:
  Safefree( RETVAL );
