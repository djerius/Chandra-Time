# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Chandra-Time.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 18;
use Test::Number::Delta;

BEGIN { use_ok('Chandra::Time') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

$t = Chandra::Time->new('1998-12-14T05:18:56.816');
delta_ok( $t->secs(), '29999936.816', "SECS");
is( $t->unix(), '913612673.632', "UNIX");
is( $t->date(), '1998:348:05:17:53.632', "DATE");
is( $t->fits(), '1998-12-14T05:18:56.816', "FITS");

$t = Chandra::Time->new('2451161.5', {format => 'jd'});
delta_ok($t->secs(),    '29980863.184'              , "SECS");
is($t->caldate(), '1998Dec14 at 00:00:00.000'    , "CALDATE");
like($t->numday(), qr/347:0:1:3\.1840*/          , "NUMDAY");
is($t->fits(2500000.1), '2132-08-30T14:25:08.184', "FITS");

$time_caldate_tai = Chandra::Time::convert(51160.9996275,
                                            { fmt_in => 'mjd',
                                              sys_in => 'tt',
                                              fmt_out => 'caldate',
                                              sys_out => 'tai',
                                            }
                                           );
is($time_caldate_tai,  '1998Dec13 at 23:58:55.632', "Convert");

$t = Chandra::Time->new(313996801.0);
delta_ok($t->secs,  313996801, "Convert secs");
is ($t->fits(),  '2007-12-14T05:20:01.000', "Back to FITS");

eval { $parse = $t->parse('1999:012:01:02:03.45'); };
if ($@) {
    diag("parse() method not available because POSIX::strptime not found");
    ok(1);
} else {

  # various platforms return slightly different results because
  # of different OS strptime implementations

    my %exp = (
	       dom   => 12,
	       dow   =>  2,
	       doy   => 12,
	       hour  =>  1,
	       min   =>  2,
	       month =>  1,
	       sec   => 3.450,
	       year  => 1999,
	      );


    if ( $^O eq 'solaris' )
    {
	delete $exp{dow};
    }

    elsif ( $^O eq 'darwin' )
    {
	delete @exp{ qw[ month dom dow ] };
    }

    ok( 0 == grep { $exp{$_} != $parse->{$_} } keys %exp );
}

# 2012 leap second checks going from FITS in UTC to 
# Chandra seconds TT
@arots_leap_test = (
    ['2012-06-30T23:59:58.5', 457488064.684],
    ['2012-06-30T23:59:59.5', 457488065.684],
    ['2012-06-30T23:59:60.5', 457488066.684],
    ['2012-07-01T00:00:00.5', 457488067.684],
    ['2012-07-01T00:00:01.5', 457488068.684]);

for $t_pair (@arots_leap_test){
    $new_secs = Chandra::Time::convert($t_pair->[0],
                                       {fmt_in => 'fits',
                                        sys_in => 'utc',
                                        fmt_out => 'secs',
                                        sys_out => 'tt'});
    ok(abs($new_secs - $t_pair->[1]) < 0.000001, $t_pair->[0] . ' UTC to SECS TT');

}
