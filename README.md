# NAME

Chandra::Time - Convert between various time formats

# VERSION

version 0.10

# SYNOPSIS

    use Chandra::Time;

    # Create a Chandra::Time object
    $t      = Chandra::Time->new('2002-05-28T01:02:03');

    # Do some converting
    $date = $t->date;  # Convert 2002-05-28T01:02:03 to YYYY:DDD:HH:MM:SS
    $secs = $t->secs;  # Time in CXC seconds (since 1998-01-01T00:00:00)
    $unix = $t->unix;  # Unix time (since 1970 Jan 1, 0 UTC)
    $unix = $t->unix('1998:241:02:14:12.233');  # Convert a different time

    # Parse into components using strptime and returns a hashref.  This works
    # by first converting to a DATE, which is in UTC.  You may be surprised
    # at the results if you input a time in SECS or FITS.  Be aware that
    # strptime implementations vary amongst platforms and your results
    # may vary.
    $parsed_time = $t->parse('1998:241:02:14:12.233');

    # Make a Julian Date time object which only accepts JD as inputs
    $t_jd  = Chandra::Time->new('2451161.5', {format => 'jd'});
    $fits  = $t_jd->fits('1998:241:02:14:12.2');  # NOPE!

# DESCRIPTION

Chandra::Time provides a simple interface to the C++ time conversion
utility axTime3 (which itself is a wrapper for XTime) written by Arnold
Rots.

The supported time formats are:

    Format   Description                                     System
   -------   ------------------------------------------      -------
     SECS    Elapsed seconds since 1998-01-01T00:00:00       TT
     NUMDAY  DDDD:hh:mm:ss.ss... Elapsed days and time       UTC
     JD      Julian Day                                      UTC
     MJD     Modified Julian Day = JD - 2400000.5            UTC
     DATE    YYYY:DDD:hh:mm:ss.ss..                          UTC
     CALDATE YYYYMonDD at hh:mm:ss.ss..                      UTC
     FITS    FITS date/time format YYYY-MM-DDThh:mm:ss.ss..  TT
     UNIX    Unix time (since 1970.0)                        UTC

Each of these formats has an associated time system, which are be one of:

    MET     Mission Elapsed Time
    TT      Terrestrial Time
    TAI     International Atomic Time
    UTC     Coordinated Universal Time

The normal usage is to create an object that allows conversion from one time
format to another.

Currently the object-oriented interface does not allow you to adjust the
input or output time system.  If you really need to do this, use the package
function convert():

# API

## Functions

- **convert**

        $time_out = Chandra::Time::convert( $time_in, \%options );

    Convert _$time\_in_ to _$time\_out_ using the specified options.

    The options are:

    - `fmt_in`
    - `sys_in`
    - `fmt_out`
    - `sys_out`

    `fmt_in` and `fmt_out` may take the following values:

        fits
        caldate
        date
        secs
        unix
        jd
        mjd
        numday

    `sys_in` and `sys_out` may take the following values:

        met
        tt
        tai
        utc

    **convert** will guess `fmt_in` and supply a default for `sys_in` if not
    specified.

## Methods

### Constructor

- **new**

        # Generic time conversion object
        $t = Chandra::Time->new();

        # Preload time into object
        $t = Chandra::Time->new($time_as_str, \%options);

    The object can either be initialized with a time, or the time to be
    converted can be given in the actual conversion output routine.

    Unless the time format is specified or it is ambiguous (i.e. SECS, JD,
    MJD, and UNIX), the time format is automatically determined.

    If the input time is a float then it is interpreted as SECS.  A different
    time format can be specified using the `format` option.

    The allowed options are:

    - `format`

        This specifies the input time format. See the `fmt_in` option to the
        **convert** function for the available formats.

### Conversions

Conversion methods by default perform a conversion on the time
specified in the constructor when the object was created.  To specify
an alternate time, pass that as an argument to the conversion method.

- **secs**

    Convert to TT seconds since 1998-01-01T00:00:00 (MET)

- **unix**

    Convert to UTC seconds since  00:00:00 UTC, January 1, 1970

- **jd**

    Convert to UTC days since JD epoch

- **mjd**

    Convert to UTC days since JD epoch

- **fits**

    Convert to TT, "YYYY-MM-DDThh:mm:ss.ss"

- **caldate**

    Convert to UTC, "YYYYMonDD at hh:mm:ss.ss"

- **date**

    Convert to UTC, "YYYY:DDD:hh:mm:ss.ss"

- **numday**

    Convert to UTC, "DDDD:hh:mm:ss.ss"

- **parse**

        %date = $t->parse( $date );

    Parse the given UTC date returning a hash with keys `sec`,
    `min`, `hour`, `mday`, `mon`, `year`, `wday`, and `yday`.

# EXPORT

None.

# BUGS

Hopefully not.  The error reporting could be improved.

# AUTHOR

Tom Aldcroft, &lt;aldcroft@localdomain>

# COPYRIGHT AND LICENSE

Copyright (C) 2005 by Tom Aldcroft

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.3 or,
at your option, any later version of Perl 5 you may have available.

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [README.axTime3 and README.XTime](https://metacpan.org/pod/README.axTime3%20and%20README.XTime)

# AUTHOR

Diab Jerius <djerius@cfa.harvard.edu>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2022 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
