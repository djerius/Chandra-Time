package Chandra::Time;

# ABSTRACT: Convert between various time formats

use 5.008000;

use strict;
use warnings;
use Carp;

our $VERSION = '0.10';

require XSLoader;
XSLoader::load('Chandra::Time', $VERSION);

my $T1998 = 883612736.816;  # Seconds from 1970:001:00:00:00 (UTC) to 1998-01-01T00:00:00 (TT)
my $FloatRE = qr/[+-]?(?:\d+[.]?\d*|[.]\d+)(?:[dDeE][+-]?\d+)?/;
our %time_style = ('fits' => { match => qr/^\d{4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}(\.\d*)?$/,
			     ax3_fmt => 'f3',
			     ax3_sys => 't',
			   },
		 'caldate' => {match => qr/^\d{4}\w{3}\d{1,2}\s+at\s+\d{1,2}:\d{1,2}:\d{1,2}(\.\d*)?$/,
			       ax3_fmt => 'c3',
			       ax3_sys => 'u'
			      },
		 'date' => { match => qr/^\d{4}:\d{1,3}:\d{1,2}:\d{1,2}:\d{1,2}(\.\d*)?/,
			     ax3_fmt => 'd3',
			     ax3_sys => 'u',
			   },
		 'secs' => { match => qr/^${FloatRE}$/,
			     ax3_fmt => 's',
			     ax3_sys => 'm',
			   },
		 'unix' => { match => qr/^${FloatRE}$/,
			     ax3_fmt => 's',
			     ax3_sys => 'u',
			   },
		 'jd'   => { match => qr/^${FloatRE}$/,
			     ax3_fmt => 'j',
			     ax3_sys => 'u',
			   },
		 'mjd'   => { match => qr/^${FloatRE}$/,
			     ax3_fmt => 'm',
			     ax3_sys => 'u',
			   },
		 'numday' => { match => qr/^\d{1,4}:\d{1,2}:\d{1,2}:\d{1,2}(\.\d*)?$/,  # DDDD:hh:mm:ss.ss.
			     ax3_fmt => 'n3',
			     ax3_sys => 'u',
			   },
		);

our @keys_time_style = qw(fits caldate date secs unix jd mjd numday);  # correct priority order

my %time_system = ('met' => 'm',  #  MET     Mission Elapsed Time ("m")
		   'tt'  => 't',  #  TT      Terrestrial Time ("t")
		   'tai' => 'a',  #  TAI     International Atomic Time ("ta" or "a")
		   'utc' => 'u'	  #  UTC     Coordinated Universal Time ("u")
		   );

# Preloaded methods go here.

##***************************************************************************
sub new {
##***************************************************************************
    my $classname = shift;

    my %opt = ref $_[-1] eq 'HASH' ? %{ pop @_ } : ();

    my $time_in = shift;
    my $self = { time_in   => $time_in,
		 %opt,
	       };
    bless($self, $classname);
    return $self;
}

##***************************************************************************
sub convert {  # Convert from/to any format
##***************************************************************************
    my $ax3_sys_in;
    my $ax3_fmt_in;
    my $ax3_sys_out;
    my $ax3_fmt_out;
    my %opt;
    local $_;

    # Grab options hash ref and make everything lower case
    %opt = %{ pop @_ } if (ref $_[-1] eq 'HASH');
    %opt = map { lc($_) => defined $opt{$_} ? lc $opt{$_} : $opt{$_} } keys %opt;

    my $time_in = shift;
    confess "Need a time\n" unless defined $time_in;

    # If fmt_in option specified and it is valid, then
    # use that to define the ax3 format and system
    if (defined (my $fmt_in = $opt{fmt_in})) {
	if (defined $time_style{ $fmt_in }) {
	    $ax3_fmt_in = $time_style{ $fmt_in }->{ax3_fmt};
	    $ax3_sys_in = $time_style{ $fmt_in }->{ax3_sys};
	    unless  ($time_in =~ /$time_style{$fmt_in}->{match}/) {
		confess "Time $time_in does not match expected format for " . uc($fmt_in) . "\n";
	    }
	} else {
	    confess "Invalid input format '$fmt_in'\n";
	}
    }
    else {			# Derive input format from data
	my $match_style;
	foreach (@keys_time_style) {
	    if ($time_in =~ /$time_style{$_}->{match}/) {
		$match_style = $_;
		last;
	    }
	}
	if (defined $match_style) {
	    $ax3_fmt_in = $time_style{ $match_style }->{ax3_fmt};
	    $ax3_sys_in = $time_style{ $match_style }->{ax3_sys};
	} else {
	    confess "Unknown format for input time\n";		# unknown format
	}
    }

    if (defined (my $sys_in = $opt{sys_in})) {
	if (defined $time_system{ $sys_in }) {
	    $ax3_sys_in = $time_system{ $opt{sys_in} };
	} else {
	    confess "Invalid input system '$sys_in'\n";
	}
    }

    if (defined (my $fmt_out = $opt{fmt_out})) {
	if (defined $time_style{ $fmt_out }) {
	    $ax3_fmt_out = $time_style{ $fmt_out }->{ax3_fmt};
	    $ax3_sys_out = $time_style{ $fmt_out }->{ax3_sys};
	} else {
	    confess "Invalid output format '$fmt_out'\n";
	}
    } else {
	$ax3_fmt_out = $time_style{secs}->{ax3_fmt};
	$ax3_sys_out = $time_style{secs}->{ax3_sys};
    }

    if (defined (my $sys_out = $opt{sys_out})) {
	if (defined $time_system{ $sys_out }) {
	    $ax3_sys_out = $time_system{ $opt{sys_out} };
	} else {
	    confess "Invalid output system '$sys_out'\n";
	}
    }

    $time_in -= $T1998 if (defined $opt{fmt_in} and $opt{fmt_in} eq 'unix');
    my $time_out = convert_time( $time_in,
			      $ax3_sys_in,
			      $ax3_fmt_in,
			      $ax3_sys_out,
			      $ax3_fmt_out,
			    );
    confess "$time_out\n" if $time_out =~ /error/i;

    $time_out += $T1998 if (defined $opt{fmt_out} and $opt{fmt_out} eq 'unix');
    return $time_out;
}

# I'm sure there's a better way, but...

##***************************************************************************
sub secs {  # TT    Float     Seconds since 1998-01-01T00:00:00
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};

    return convert($time, { fmt_in => $self->{format},
			    fmt_out => 'secs',
			    sys_out => 'met'
			  }
		  );
}

##***************************************************************************
sub unix {  # UTC    Float  Seconds since  00:00:00 UTC, January 1, 1970
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			 fmt_out => 'unix',
			 sys_out => 'utc'
		       }
		  );
}

##***************************************************************************
sub jd {  # UTC    Float     Days since JD epoch
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			    fmt_out => 'jd',
			    sys_out => 'utc'
			  }
		  );
}

##***************************************************************************
sub mjd {  # UTC    Float     Days since JD epoch
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			    fmt_out => 'mjd',
			    sys_out => 'utc'
			  }
		  );
}

##***************************************************************************
sub fits {			# TT     YYYY-MM-DDThh:mm:ss.ss
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			 fmt_out => 'fits',
			 sys_out => 'tt'
		       }
		  );
}

##***************************************************************************
sub caldate {  # UTC  YYYYMonDD at hh:mm:ss.ss
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			 fmt_out => 'caldate',
			 sys_out => 'utc'
		       }
		  );
}

##***************************************************************************
sub date {   # UTC    YYYY:DDD:hh:mm:ss.ss
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			 fmt_out => 'date',
			 sys_out => 'utc'
		       }
		  );
}

##***************************************************************************
sub numday {   # UTC    DDDD:hh:mm:ss.ss
##***************************************************************************
    my $self = shift;
    my $time = @_ ? shift : $self->{time_in};
    return convert($time, { fmt_in => $self->{format},
			 fmt_out => 'numday',
			 sys_out => 'utc'
		       }
		  );
}

##***************************************************************************
sub parse {
# Return a hash of values from strptime.  This is in the UTC system.
# ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday) = POSIX::strptime(string, format);
##***************************************************************************
    my $self = shift;
    require POSIX::strptime;

    my $date = $self->date(@_);
    my $frac = ($date =~ s/(\.\d*)$//) ? $1 : '';
    my @vals = POSIX::strptime($date, '%Y:%j:%H:%M:%S');
    return { sec => $vals[0] . $frac,
	     min => $vals[1],
	     hour => $vals[2],
	     dom => $vals[3],  # day of month
	     month => defined $vals[4] ? $vals[4] + 1 : undef,
	     year => defined $vals[5] ? $vals[5] + 1900 : undef,
	     dow => $vals[6],
	     doy => $vals[7] + 1,
	   };
}

1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

  use Chandra::Time;

  # Create a Chandra::Time object
  $t      = Chandra::Time->new('2002-05-28T01:02:03');

  # Do some converting
  $date = $t->date;  # Convert 2002-05-28T01:02:03 to YYYY:DDD:HH:MM:SS
  $secs = $t->secs;  # Time in CXC seconds (since 1998-01-01T00:00:00)
  $unix = $t->unix;  # Unix time (since 1970 Jan 1, 0 UTC)
  $unix = $t->unix('1998:241:02:14:12.233');  # Convert a different time

  # Parse into components using strptime and returns a hashref.  This works
  # by first converting to a DATE, which is in UTC.  You may be surprised
  # at the results if you input a time in SECS or FITS.  Be aware that
  # strptime implementations vary amongst platforms and your results
  # may vary.
  $parsed_time = $t->parse('1998:241:02:14:12.233');

  # Make a Julian Date time object which only accepts JD as inputs
  $t_jd  = Chandra::Time->new('2451161.5', {format => 'jd'});
  $fits  = $t_jd->fits('1998:241:02:14:12.2');  # NOPE!

=head1 DESCRIPTION

Chandra::Time provides a simple interface to the C++ time conversion
utility axTime3 (which itself is a wrapper for XTime) written by Arnold
Rots.

The supported time formats are:

 Format   Description                                     System
-------   ------------------------------------------      -------
  SECS    Elapsed seconds since 1998-01-01T00:00:00       TT
  NUMDAY  DDDD:hh:mm:ss.ss... Elapsed days and time       UTC
  JD      Julian Day                                      UTC
  MJD     Modified Julian Day = JD - 2400000.5            UTC
  DATE    YYYY:DDD:hh:mm:ss.ss..                          UTC
  CALDATE YYYYMonDD at hh:mm:ss.ss..                      UTC
  FITS    FITS date/time format YYYY-MM-DDThh:mm:ss.ss..  TT
  UNIX    Unix time (since 1970.0)                        UTC

Each of these formats has an associated time system, which are be one of:

  MET     Mission Elapsed Time
  TT      Terrestrial Time
  TAI     International Atomic Time
  UTC     Coordinated Universal Time

The normal usage is to create an object that allows conversion from one time
format to another.

Currently the object-oriented interface does not allow you to adjust the
input or output time system.  If you really need to do this, use the package
function convert():




=head1 API

=head2 Functions

=over

=item B<convert>

  $time_out = Chandra::Time::convert( $time_in, \%options );

Convert I<$time_in> to I<$time_out> using the specified options.

The options are:

=over

=item C<fmt_in>

=item C<sys_in>

=item C<fmt_out>

=item C<sys_out>

=back

C<fmt_in> and C<fmt_out> may take the following values:

  fits
  caldate
  date
  secs
  unix
  jd
  mjd
  numday

C<sys_in> and C<sys_out> may take the following values:

  met
  tt
  tai
  utc

B<convert> will guess C<fmt_in> and supply a default for C<sys_in> if not
specified.

=begin pod-coverage

=item convert_time

=end pod-coverage

=back

=head2 Methods

=head3 Constructor

=over

=item B<new>

  # Generic time conversion object
  $t = Chandra::Time->new();

  # Preload time into object
  $t = Chandra::Time->new($time_as_str, \%options);

The object can either be initialized with a time, or the time to be
converted can be given in the actual conversion output routine.

Unless the time format is specified or it is ambiguous (i.e. SECS, JD,
MJD, and UNIX), the time format is automatically determined.

If the input time is a float then it is interpreted as SECS.  A different
time format can be specified using the C<format> option.

The allowed options are:

=over

=item C<format>

This specifies the input time format. See the C<fmt_in> option to the
B<convert> function for the available formats.

=back

=back

=head3 Conversions

Conversion methods by default perform a conversion on the time
specified in the constructor when the object was created.  To specify
an alternate time, pass that as an argument to the conversion method.

=over

=item B<secs>

Convert to TT seconds since 1998-01-01T00:00:00 (MET)

=item B<unix>

Convert to UTC seconds since  00:00:00 UTC, January 1, 1970

=item B<jd>

Convert to UTC days since JD epoch

=item B<mjd>

Convert to UTC days since JD epoch

=item B<fits>

Convert to TT, "YYYY-MM-DDThh:mm:ss.ss"

=item B<caldate>

Convert to UTC, "YYYYMonDD at hh:mm:ss.ss"

=item B<date>

Convert to UTC, "YYYY:DDD:hh:mm:ss.ss"

=item B<numday>

Convert to UTC, "DDDD:hh:mm:ss.ss"

=item B<parse>

  %date = $t->parse( $date );

Parse the given UTC date returning a hash with keys C<sec>,
C<min>, C<hour>, C<mday>, C<mon>, C<year>, C<wday>, and C<yday>.

=back

=head1 EXPORT

None.

=head1 BUGS

Hopefully not.  The error reporting could be improved.

=head1 SEE ALSO

README.axTime3 and README.XTime

=head1 AUTHOR

Tom Aldcroft, E<lt>aldcroft@localdomainE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2005 by Tom Aldcroft

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
